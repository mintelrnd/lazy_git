import os
import sh
import sys
import json
import time
import lazy_git
from textwrap import dedent
from signal import SIGTERM
from threading import Thread
from unittest import TestCase
from tempfile import TemporaryDirectory, gettempdir
from urllib.request import urlopen
from urllib.error import URLError
from subprocess import Popen, PIPE, STDOUT
from socket import socket, AF_INET, SOCK_STREAM
from lazy_git import __version__ as API_VERSION


def _get_port():
    """ Hacky helper function to get a random unused port """

    s = socket(AF_INET, SOCK_STREAM)
    s.bind(('localhost', 0))
    _, port = s.getsockname()
    s.close()
    return port


class ApiRepo(object):
    """ Encapsulate a temporary Git local working copy. """

    def __init__(self):
        self._dir = TemporaryDirectory()
        self._git = sh.git.bake(_cwd=self._dir.name)
        self._git.init()

    @property
    def dir(self):
        return self._dir.name

    @property
    def name(self):
        return os.path.basename(self._dir.name)

    def add(self, files, message='Test commit'):
        for data in files:
            if isinstance(data, tuple):
                path = os.path.join(self._dir.name, data[0])
                content = data[1]
            else:
                path = os.path.join(self._dir.name, data)
                content = ''

            if not os.path.isdir(os.path.dirname(path)):
                os.makedirs(os.path.dirname(path))

            with open(path, 'w') as f:
                f.write(content)

            self._git.add(path)

        self._git.commit(message=message)


class ApiProcess(object):
    """ Encapsulate the subprocess management for the API service """

    def __init__(self, repo):
        self._port = _get_port()
        self._repo = repo

    def start(self):
        code = dedent("\
            from lazy_git import create_app; \
            app = create_app(repo_dir='{}'); \
            app.run(host='localhost', port={})"\
        .format(gettempdir(), self._port))

        # Create a copy of the current process' environment
        sub_env = os.environ.copy()
        # Grab the base of the lazy_git module
        lazy_git_dir = os.sep.join(lazy_git.__file__.split(os.sep)[:-2])
        py_path = sub_env.get("PYTHONPATH", "")
        # Prepend lazy_git module to subprocess's pythonpath
        sub_env["PYTHONPATH"] = ":".join((lazy_git_dir, py_path))

        self._proc = Popen('{} -c "{}"'.format(sys.executable, code), 
            shell=True, stdout=PIPE, stderr=STDOUT, preexec_fn=os.setsid,
            env = sub_env)

    def stop(self):

        class CaptureThread(Thread):
            def __init__(self, proc):
                Thread.__init__(self)
                self._proc = proc
            def run(self):
                self.out, _ = self._proc.communicate()

        thread = CaptureThread(self._proc)
        thread.start()

        # Kill subprocess and shell
        os.killpg(os.getpgid(self._proc.pid), SIGTERM)

        # Print output (which is captured by Nose)
        thread.join()
        print(thread.out)

    @property
    def port(self):
        return self._port

    @property
    def root(self):
        return 'http://localhost:{}'.format(self.port)

    @property
    def url(self):
        return '{}/api/v{}'.format(self.root, API_VERSION)


class ApiTestCase(TestCase):
    """ Base level test case for API tests """

    def setUp(self):
        self._repo = ApiRepo()
        self._api = ApiProcess(self._repo)
        self._api.start()

        # Wait for subprocess to come-up
        for _ in range(100):
            try:
                with urlopen('{}/info'.format(self._api.root)) as resp:
                    version = json.loads(resp.read().decode('utf-8'))['version']
                    assert version == API_VERSION
                break

            except URLError as e:
                if isinstance(e.reason, ConnectionRefusedError):
                    time.sleep(0.01)
                else:
                    raise e

    def tearDown(self):
        self._api.stop()

    @property
    def repo(self):
        return self._repo

    @property
    def api(self):
        return self._api
