import json
from urllib.request import urlopen
from lazy_git.api.tests import ApiTestCase


def get_node(content, name):
    return next((node for node in content if node['name'] == name))

def node_exists(content, name):
    return any(node['name'] == name for node in content)

def is_tree_node(content, name):
    return get_node(content, name)['type'] == 'tree'

def is_blob_node(content, name):
    return get_node(content, name)['type'] == 'blob'


class TestBrowseApi(ApiTestCase):

    def test_get_content(self):
        """ Test GET queries to discover and return contents of text files."""

        self.repo.add([('a.txt', 'TEST_A'),
                       ('b.txt', 'TEST_B')])

        # Request and check flat tree list structure
        with urlopen('{}/browse/{}'.format(self.api.url, self.repo.name)) as resp:
            obj = json.loads(resp.read().decode('utf-8'))
            assert(obj['type'] == 'tree')

            assert('content' in obj)
            content = obj['content']
            assert(len(content) == 2)
            assert(node_exists(content, 'a.txt'))
            assert(is_blob_node(content, 'a.txt'))
            assert(node_exists(content, 'b.txt'))
            assert(is_blob_node(content, 'b.txt'))

        # Request and check contents of both files
        for f, txt in [('a.txt', 'TEST_A'), ('b.txt', 'TEST_B')]:
            with urlopen('{}/browse/{}/{}'.format(self.api.url, self.repo.name,
                f)) as resp:
                obj = json.loads(resp.read().decode('utf-8'))
                print(json.dumps(obj, indent=2))

                assert(obj['type'] == 'blob')
                assert(obj['content'] == txt)

    def test_get_depth(self):
        """ Test functionality of 'depth' GET parameter. """

        # TODO: Make this test recursive and parameterized on depth (overkill?)

        self.repo.add(['0.txt',
                       '1/1.txt',
                       '1/2/2.txt',
                       '1/2/3/3.txt',
                       '1/2/3/4/4.txt'])

        # Request and check 3 level deep tree
        with urlopen('{}/browse/{}?depth=3'.format(self.api.url, 
            self.repo.name)) as resp:
            obj = json.loads(resp.read().decode('utf-8'))
            print(json.dumps(obj, indent=2))

            assert(obj['type'] == 'tree')

            # /
            assert('content' in obj)
            content = obj['content']
            assert(len(content) == 2)
            assert(node_exists(content, '0.txt'))
            assert(is_blob_node(content, '0.txt'))
            assert(node_exists(content, '1'))
            assert(is_tree_node(content, '1'))

            # /1
            assert('content' in get_node(content, '1'))
            content = get_node(content, '1')['content']
            assert(len(content) == 2)
            assert(node_exists(content, '1.txt'))
            assert(is_blob_node(content, '1.txt'))
            assert(node_exists(content, '2'))
            assert(is_tree_node(content, '2'))

            # /1/2
            assert('content' in get_node(content, '2'))
            content = get_node(content, '2')['content']
            assert(len(content) == 2)
            assert(node_exists(content, '2.txt'))
            assert(is_blob_node(content, '2.txt'))
            assert(node_exists(content, '3'))
            assert(is_tree_node(content, '3'))

            # /1/2/3
            assert('content' in get_node(content, '3'))
            content = get_node(content, '3')['content']
            assert(len(content) == 2)
            assert(node_exists(content, '3.txt'))
            assert(is_blob_node(content, '3.txt'))
            assert(node_exists(content, '4'))
            assert(is_tree_node(content, '4'))

            # No more levels (explicitly omit level 4)
            assert('content' not in get_node(content, '4'))

    def test_multiple_tree(self):
        """ Test ability to cope with multiple tree nodes. """

        self.repo.add(['1/a.txt',
                       '1/b.txt',
                       '1/2a/a.txt',
                       '1/2b/b.txt'])

        # Request full tree
        with urlopen('{}/browse/{}?depth=3'.format(self.api.url, 
            self.repo.name)) as resp:
            obj = json.loads(resp.read().decode('utf-8'))
            print(json.dumps(obj, indent=2))

            # /
            assert('content' in obj)
            content = obj['content']
            assert(len(content) == 1)
            assert(node_exists(content, '1'))
            assert(is_tree_node(content, '1'))

            # /1
            assert('content' in get_node(content, '1'))
            content1 = obj['content'][0]['content']
            assert(len(content1) == 4)
            assert(node_exists(content1, 'a.txt'))
            assert(is_blob_node(content1, 'a.txt'))
            assert(node_exists(content1, 'b.txt'))
            assert(is_blob_node(content1, 'b.txt'))
            assert(node_exists(content1, '2a'))
            assert(is_tree_node(content1, '2a'))
            assert(node_exists(content1, '2b'))
            assert(is_tree_node(content1, '2b'))

            # /1/2a
            assert('content' in get_node(content1, '2a'))
            content = get_node(content1, '2a')['content']
            assert(len(content) == 1)
            assert(node_exists(content, 'a.txt'))
            assert(is_blob_node(content, 'a.txt'))

            # /1/2b
            assert('content' in get_node(content1, '2b'))
            content = get_node(content1, '2b')['content']
            assert(len(content) == 1)
            assert(node_exists(content, 'b.txt'))
            assert(is_blob_node(content, 'b.txt'))

