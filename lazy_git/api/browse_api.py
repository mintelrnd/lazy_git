import os
import sh
import re
from copy import copy
from flask import jsonify, current_app, abort, request
from flask.views import MethodView


class BrowseApi(MethodView):
    """
    Browse API method view.

    This class implements the Flask MethodView interface for supporting RESTful
    calls over HTTP.
    """

    methods = [ 'GET' ]

    def get(self, repo_name, obj_path):
        """
        Support the GET RESTful HTTP call.

        This API call renders the contents of a Git repository to JSON and 
        returns it over HTTP. The call supports the following URI format:

        ``<repo_name>/<git_treeish>:<path>?depth=<depth>``

        ``repo_name`` should match a checkout out repository in the directory
        specified in the REPO_DIR configuration option (either a normal clone or
        a bare clone with the '.git' extension).

        ``git_treeish`` should be the treeish for the version of the object you 
        wish to view (such as a branch name or a commit ID).  This is optional 
        and if it is not specified the HEAD is used as the default.

        ``path`` is the absolute location (within the repository) to the object 
        you wish to view.  If the object is a blob then the contents of the 
        object is rendered to the value of 'content' in the returned JSON; if 
        the object is a tree then the directory structure (up to the depth 
        specified by ``depth``, see below) is recusively rendered to the value 
        of 'content' in the returned JSON.

        ``depth`` is an optional parameter that may be use to limit or extend 
        the desired recursive depth of the rendering of a tree object.  The 
        option has no effect if a blob object is rendered.

        :param repo_name: The name of the repository on disk.
        :param obj_path: The path to a Git object.
        """

        # Add HEAD: to path by default
        if not re.match('.*\:.*', obj_path):
            obj_path = 'HEAD:{}'.format(obj_path)

        # Assume normal directory
        repo_dir = os.path.join(current_app.config['REPO_DIR'], repo_name)

        # Look for bare repository
        if not os.path.isdir(repo_dir):
            repo_dir = '{}.git'.format(repo_dir)

        # 404 if repository can't be found
        if not os.path.isdir(repo_dir):
            abort(404)

        # Set current directory to bare repo
        git = sh.git.bake(_cwd=repo_dir)

        # Get object type
        try:
            obj_type = str(git('cat-file', '-t', obj_path)).rstrip()
        except sh.ErrorReturnCode_128 as e:
            if "Not a valid object name" in e.stderr:
                abort(404)
            raise e

        # Recursively create tree nodes
        def populate_tree(obj_data, obj_content, depth):
            obj_nodes = obj_data[3].split('/')

            # Not at leaf node
            if len(obj_nodes) > 1:

                # Look for existing node or create default
                node = next((node for node in obj_content 
                    if node['name'] == obj_nodes[0]), 
                    { 'name': obj_nodes[0] })

                if depth:

                    # Create content list if it does not exist
                    if 'content' not in node:
                        node['content'] = []

                    # Remove one level and recurse
                    obj_data[3] = '/'.join(obj_nodes[1:])
                    populate_tree(obj_data, node['content'], depth-1)

            # Leaf node, add data
            else:
                obj_content.append({
                    'permissions': obj_data[0],
                    'type': obj_data[1],
                    'treeish': obj_data[2],
                    'name': obj_nodes[0] })


        # Tree contains multiple objects, so scan (default 3 depth)
        if obj_type == "tree":
            depth = int(request.args.get('depth', 3))
            obj_list = [obj.split() for obj in git('ls-tree', '-rt', obj_path)]

            # Create tree structure
            obj_content = []
            for obj_data in obj_list:
                populate_tree(obj_data, obj_content, depth)

        # Print blob content directly
        elif obj_type == "blob":
            obj_content = str(git('cat-file', '-p', obj_path))

        # Cannot browse other object types
        else:
            abort(404)

        return jsonify({ 'type': obj_type, 'content': obj_content })
