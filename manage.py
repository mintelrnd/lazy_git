from flask_script import Server, Manager
from lazy_git import create_app

_DFLT_PORT = 65004

manager = Manager(create_app)
manager.add_option('-c', '--config', dest='config', required=False)

@manager.command
def runserver(port=_DFLT_PORT,
              host='127.0.0.1',
              debug=False):
    """ Run a Tornado based server. """

    from tornado.wsgi import WSGIContainer
    from tornado.httpserver import HTTPServer
    from tornado.ioloop import IOLoop

    # Enable debug mode
    if debug:
        manager.app.config['DEBUG'] = True

    # Enable Tornado debug options
    if manager.app.config['DEBUG']:
        import tornado.log
        import tornado.autoreload
        tornado.log.enable_pretty_logging()
        tornado.autoreload.start(IOLoop.instance())

    # Wrap Flask app and run in Tornado
    http_server = HTTPServer(WSGIContainer(manager.app))
    http_server.listen(str(port), str(host))
    IOLoop.instance().start()

if __name__ == '__main__':
    manager.run()
